// Copyright (c) 2018-2019 The Go-Turbo Project
// Copyright (c) 2016-2017 Mathias J. Hennig
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package system

// Standard packages are documented at https://golang.org/pkg/.
import (
	"flag"
	"io"
	"os"
	"os/exec"
	"path"
	"testing"
)

// TestName is a smoke test for the system.Name function.
func TestName(context *testing.T) {

	if name := Name(); name != path.Base(Args[0]) {

		context.Logf("system.Args == %v", Args)
		context.Logf("system.Name() == %v", name)

		context.Error("system.Name() != path.Base(system.Args[0])")
	}
}

// A smoke test for the system.Status function.
func TestStatus(context *testing.T) {

	// Some errors should not be considered failures.
	for err := range []error{io.EOF, nil} {

		if code := Status(nil); code != ExitSuccess {
			context.Errorf("ExitSuccess != Status(%v)", err)
		}
	}

	// Invalid commands should fail.
	expect(context, ExitFailure, "invalidcommand")
	expect(context, ExitFailure, "cd", "/path/to/nonexistent/directory")

	// Senseless commands should not be successful.
	reject(context, ExitSuccess, os.Args[0], "-unrecognizedoption")
	reject(context, ExitSuccess, "dir", "/path/to/nonexistent/resource")

	// Valid commands should be successful.
	expect(context, ExitSuccess, "dir")
	expect(context, ExitSuccess, "echo", "hello world")

	if code := Status(flag.ErrHelp); code != ExitWarning {
		context.Errorf("Status(flag.ErrHelp) != ExitWarning")
	}
}

// Used internally to verify execution codes.
func expect(context *testing.T, code int, command string, options ...string) {

	cmd := exec.Command(command, options...)
	err := cmd.Run()

	if result := Status(err); result != code {
		context.Errorf("%v != %v  # %s", code, result, command)
	}
}

// Used internally to verify execution codes.
func reject(context *testing.T, code int, command string, options ...string) {

	cmd := exec.Command(command, options...)
	err := cmd.Run()

	if result := Status(err); result == code {
		context.Errorf("%v == %v  # %s", code, result, command)
	}
}
