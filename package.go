// Copyright (c) 2018-2019 The Go-Turbo Project
// Copyright (c) 2016-2017 Mathias J. Hennig
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Package system eases the creation of realiable executables.
package system

// Standard packages are documented at https://golang.org/pkg/.
import (
	"flag"
	"io"
	"os"
	"os/exec"
	"path"
	"syscall"
)

// Constant group Exit* defines common exit status codes for commands.
const (
	ExitSuccess = iota
	ExitWarning
	ExitFailure
	ExitUnknown
)

// Package system exposes a set of variables that proxy resources any program
// commonly interacts with, each of which defaults to the exact same value as
// its respective counterpart:
//
//	func main() {
//	    status := do.Something(system.Args, system.Stdin, system.Stdout)
//	    system.Exit(status)
//	}
//
// Being able to manipulate these symbols without touching the original runtime
// resources eases the creation of tests, even ones that cover function main:
//
//	defer system.Reset()
//
//	system.Args = []string{"example", "-h"}
//	system.Exit = func(status int) {
//
//	    if status != system.ExitSuccess {
//	        test.Errorf("main(%v) exited with %v", system.Args, status)
//	    }
//	}
//
//	main()
//
// One can also provide input data and examine the output if necessary, or
// explicitly void in- and output:
//
//	system.Stdin = ioutil.NopCloser(bytes.NewReader(nil))
//	system.Stdout = ioutil.NopCloser(ioutil.Discard)
//
// Applications outside the test domain seem to be rare, however.
var (
	Args                  = os.Args
	Exit                  = os.Exit
	Stderr io.WriteCloser = os.Stderr
	Stdin  io.ReadCloser  = os.Stdin
	Stdout io.WriteCloser = os.Stdout
)

// Name returns the basename(1) of the first system.Args item.
func Name() string {

	name := path.Base(Args[0])
	return name
}

// Ok returns true if the given error object equals io.EOF or is nil.
func Ok(err error) bool {

	return err == nil || err == io.EOF
}

// Reset restores the public system variables to the (current) values of their
// respective counterparts.
func Reset() {

	Args = os.Args
	Exit = os.Exit
	Stderr = os.Stderr
	Stdin = os.Stdin
	Stdout = os.Stdout
}

// Status computes an exit code based on the given error and suitable for use
// with function os.Exit. In case the former is an exec.ExitError instance, the
// original command's status code is returned:
//
//	result := exec.Command("example").Run()
//	code := system.Status(result)
//	fmt.Printf("example command exited with %d", code)
//
// The returned code always equals system.ExitSuccess for both io.EOF and nil:
//
//	system.ExitSuccess == system.Status(io.EOF)
//	system.ExitSuccess == system.Status(nil)
//
// Any other error leads to system.ExitFailure being returned:
//
//	err := errors.New("example")
//	system.ExitFailure == system.Status(err)
//
// Note that in the function may explicitly return system.ExitUnknown; e.g.
// when fetching the syscall.WaitStatus from an exec.ExitError is not possible,
// i.e. due to Go implementation bits specific to the operating system.
func Status(err error) int {

	var code = ExitUnknown
	var exit *exec.ExitError
	var ok bool
	var status syscall.WaitStatus

	if Ok(err) {
		code = ExitSuccess
	} else if err == flag.ErrHelp {
		code = ExitWarning
	} else if exit, ok = err.(*exec.ExitError); !ok {
		code = ExitFailure
	} else if status, ok = exit.Sys().(syscall.WaitStatus); ok {
		code = status.ExitStatus()
	}

	return code
}

// Function init is invoked automatically during startup, setting the package
// variables to their respective defaults using the system.Reset function.
func init() {

	Reset()
}
